FROM node:8.2.1

# Create app directory
WORKDIR /usr/src/app

COPY package.json .

RUN yarn --prod

# Bundle app source
COPY src .

EXPOSE 8000
CMD [ "yarn", "start" ]
