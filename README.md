## Stretch Assignment #3

### Notes
* The app inserts some test data in the database on startup. 
* You can add users and traits using the examples below.

### To Run
1. run `docker-compose up`
2. Use Postman/curl to send requests.

### Query Examples

**To add a user**

* Method: Post
* URL: http://localhost:8000/user/new-user
* Example data:


    {
      'user': {
          'name':       'Shawn
          , 'isActive': true
      },
      'traitScores': [
        {
          'trait':   'FooBar'
          , 'score': 6
        }
      ]
    }
    
**To query by trait**

* Method: GET
* URL: http://localhost:8000/user/find-by-trait/Passionate

**To update a user's traits**

* Method: PUT
* URL: http://localhost:8000/user/update-user-trait-scores
* Example data:

   
    {
    	"user": {
    		"_id": "5a52a3000b439524f961d728"
    	},
    	"traitScores": [
    		{
    	"trait": "FooBar"
    	, "score": 10
    	}]
    }