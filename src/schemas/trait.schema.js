const mongoose = require('mongoose')
		, Schema   = mongoose.Schema;

const TraitSchema = new Schema({
	Name: String
});

module.exports = mongoose.model('Trait', TraitSchema);