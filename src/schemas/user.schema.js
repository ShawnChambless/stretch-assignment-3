const mongoose = require('mongoose')
		, Schema   = mongoose.Schema;

const UserSchema = new Schema({
	Name:          String
	, CreatedDate: { type: Date, index: true, default: Date.now }
	, IsActive:    { type: Boolean, default: true }
	, TraitScores: [ { type: Schema.Types.ObjectId, ref: 'TraitScore', index: true, default: [] } ]
});

module.exports = mongoose.model('User', UserSchema);