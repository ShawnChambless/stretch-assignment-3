const mongoose = require('mongoose')
		, Schema   = mongoose.Schema;

const TraitScoreSchema = new Schema({
	AppUser:       { type: Schema.Types.ObjectId, ref: 'user', required: true }
	, Score:       { type: Number, required: true }
	, CreatedDate: { type: Date, index: true, default: Date.now }
	, IsValid:     { type: Boolean, default: true }
	, Trait:       { type: String, required: true }
});

module.exports = mongoose.model('TraitScore', TraitScoreSchema);