module.exports = {
	shawn: {
		user:        {
			name: 'Shawn'
		},
		traitScores: [
			{
				trait: 'FooBar',
				score: 6
			},
			{
				trait: 'Dependable',
				score: 10
			},
			{
				trait: 'Passionate',
				score: 10
			}
		]
	},
	otherGuy:
				 {
					 user:        {
						 name: 'The Competition'
					 },
					 traitScores: [
						 {
							 trait: 'FooBar',
							 score: 7
						 },
						 {
							 trait: 'Not Shawn',
							 score: 10
						 },
						 {
							 trait: 'Dependable',
							 score: 4
						 },
						 {
							 trait: 'Passionate',
							 score: 3
						 }
					 ]
				 }
};