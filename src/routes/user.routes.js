const express    = require('express')
		, mongoose   = require('mongoose')
		, UserRouter = express.Router()
		, User       = require('./../schemas/user.schema')
		, TraitScore = require('./../schemas/traitScore.schema')
		, _          = require('lodash')
		, util       = require('util');

UserRouter.post('/new-user', createUser);
UserRouter.put('/update-user-trait-scores', updateUserTraitScores);
UserRouter.get('/find-by-trait/:trait', getUsersByTrait);

module.exports = UserRouter;

async function insertNewTraits(traits, userId, res) {
	const newTraitScores = _.map(traits, traitScore => {
		return {
			insertOne: {
				document: new TraitScore({
					AppUser: userId
					, Trait: traitScore.trait
					, Score: traitScore.score
				})
			}
		};
	});
	
	return TraitScore.bulkWrite(newTraitScores)
			.then((resp, err) => {
				if(err) return handleError(err, res);
				else return _.values(resp.insertedIds);
			});
}

function handleError(err, res) {
	console.log(err);
	return res.status(500).json(err);
}

function createUser(req, res) {
	const user = req.body.user;
	User.findOne({ Name: user.name })
			.then((existingUser, err) => {
						if(err) return handleError(err, res);
						if(existingUser !== null) return res.status(200).json('user exists');
						else {
							const userId = new mongoose.Types.ObjectId();
							insertNewTraits(req.body.traitScores, userId, res)
									.then(traitIds => {
										return new User({
											_id:           userId
											, Name:        user.name
											, TraitScores: traitIds
											, IsActive:    user.isActive
										})
												.save()
												.then((newUser, err) => {
													if(err) return handleError(err, res);
													return res.status(201).json(newUser);
												});
									});
						}
					}
			);
}

function updateUserTraitScores(req, res) {
	insertNewTraits(req.body.traitScores, req.body.user._id, res)
			.then(traitIds => {
				User.findOneAndUpdate({ _id: req.body.user._id }, { $push: { TraitScores: { $each: traitIds } } }, { new: true })
						.then((updatedUser, err) => {
							if(err) return handleError(err, res);
							else return res.status(200).json(updatedUser);
						});
			});
}

function getUsersByTrait(req, res) {
	TraitScore.aggregate([
		{ $match: { $and: [ { Trait: req.params.trait }, { IsValid: true }, { Score: { $gt: 3 } } ] } },
		{ $sort: { CreatedDate: -1 } },
		{
			$lookup: {
				from:           'users'
				, localField:   'AppUser'
				, foreignField: '_id'
				, as:           'User'
			}
		},
		{ $unwind: '$User' },
		{
			$group: {
				_id:                '$User.Name'
				, traitId:          { $first: '$_id' }
				, traitName:        { $first: '$Trait' }
				, traitCreatedDate: { $first: '$CreatedDate' }
				, traitScore:       { $first: '$Score' }
				, userData:         { $first: '$User' }
			}
		},
		{ $sort: { traitScore: -1 } }
	])
			.exec()
			.then((users, err) => {
				if(err) return handleError(err, res);
				else return res.status(200).json(users);
			});
}