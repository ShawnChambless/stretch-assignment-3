const express    = require('express')
		, app        = express()
		, port       = 8000
		, bodyParser = require('body-parser')
		, UserRoutes = require('./routes/user.routes')
		, mongoose   = require('mongoose')
		, userData   = require('./userData')
		, axios      = require('axios')
		, dbHost     = process.env.MONGO_HOST || 'localhost';

mongoose.connect(`mongodb://${dbHost}/stretch-assignment-3`);

app.use(bodyParser.json());
app.use('/user', UserRoutes);

app.listen(port, () => {
	console.log(`Server listening on ${port}`);
	
	axios.all([ axios.post('http://localhost:8000/user/new-user', userData.shawn), axios.post('http://localhost:8000/user/new-user', userData.otherGuy) ])
			.then(axios.spread(() => {
				console.log('Inserted Shawn\'s data');
				console.log('Inserted Other Guy\'s data');
			}));
});